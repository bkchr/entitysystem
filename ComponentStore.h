#ifdef PRAGMA_ONCE_ENABLED
#   pragma once
#endif

#ifndef H_COMPONENTSTORE_INCLUDED_20121229155331
#define H_COMPONENTSTORE_INCLUDED_20121229155331

namespace ES
{
	class ComponentStore
	{
	public:
		virtual ~ComponentStore() {}
	};

	template<typename Component>
	class ConcreteComponentStore : public ComponentStore
	{
	public:

		template<typename ...Arguments>
		ConcreteComponentStore(Arguments &&...args) :
#if defined(VSBUGFIXED) || defined(__GNUC__)
			mComponent(std::forward<Arguments>(args)...)
#else
			mComponent(args...)
#endif
		{}

		Component &GetComponent() { return mComponent; }

	private:
		Component mComponent;
	};
}   //namespace ES

#endif //#ifndef H_COMPONENTSTORE_INCLUDED_20121229155331
