#ifndef ENTITY_H
#define ENTITY_H

#include "EntitySystemDefines.h"
#include "ComponentStore.h"
#include "ComponentID.h"

namespace ES
{
    class ComponentFromEnumBase
    {};

	template<unsigned long Value>
    class ComponentFromEnum : public ComponentFromEnumBase
	{};

	class Entity
	{
	public:
		Entity(const Entity &) = delete;
		Entity(const unsigned long id) :
			mComponentsDirty(false),
			mID(id)
		{}

		~Entity()
		{
			for(auto itr(mComponents.begin()), end(mComponents.end()); itr != end; ++itr)
				delete itr->second;

			mComponents.clear();
		}

		const ComponentContainer &GetComponents() const { return mComponents; }

		template<typename Component, typename ... Arguments>
		void AddComponent(Arguments &&... args)
		{
			// could be removed if a multimap is used!
			auto comitr = mComponents.find(ComponentIDGenerator<Component>::Get());

			if(comitr != mComponents.end())
				throw "The entity already has a component with the same type!";

#if defined(VSBUGFIXED) || defined(__GNUC__)
			mComponents[ComponentIDGenerator<Component>::Get()] = new ConcreteComponentStore<Component>(std::forward<Arguments>(args)...);
#else
			mComponents[ComponentIDGenerator<Component>::Get()] = new ConcreteComponentStore<Component>(args...);
#endif

			mComponentsDirty = true;
		}

		template<unsigned long Component>
		void AddComponent()
		{
			AddComponent<ComponentFromEnum<Component>>();
		}

		template<typename Component>
		Component &GetComponent()
		{
			auto comitr = mComponents.find(ComponentIDGenerator<Component>::Get());

			assert(comitr != mComponents.end() && "Component doesn't exist!");

			return static_cast<ConcreteComponentStore<Component>*>(comitr->second)->GetComponent();
		}

		template<typename Component>
		void RemoveComponent()
		{
			auto comitr = mComponents.find(ComponentIDGenerator<Component>::Get());

			if(comitr == mComponents.end())
				return;

			delete comitr->second;
			mComponents.erase(comitr);
			mComponentsDirty = true;
		}

		template<unsigned long Component>
		void RemoveComponent()
		{
			RemoveComponent<ComponentFromEnum<Component>>();
		}

		bool ComponentsDirty(bool reset = false)
		{
			bool retval = mComponentsDirty;

			if(reset)
				mComponentsDirty = true;

			return retval;
		}

        bool operator==(const Entity &other) const
		{
			return other.mID == mID;
		}

	private:
		unsigned long mID;
		ComponentContainer mComponents;
		bool mComponentsDirty;
	};

}

#endif // ENTITY_H
