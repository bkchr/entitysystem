#ifdef PRAGMA_ONCE_ENABLED
#   pragma once
#endif

#ifndef H_ENTITYSYSTEMDEFINES_INCLUDED_20121230165602
#define H_ENTITYSYSTEMDEFINES_INCLUDED_20121230165602

#include <unordered_map>
#include <assert.h>

namespace ES
{
	typedef unsigned long ComponentID;

	class ComponentStore;
	typedef std::unordered_map<ComponentID, ComponentStore*> ComponentContainer;
}   //namespace ES

#endif //#ifndef H_ENTITYSYSTEMDEFINES_INCLUDED_20121230165602
