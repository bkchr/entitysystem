#ifdef PRAGMA_ONCE_ENABLED
#   pragma once
#endif

#ifndef H_ENTITYSYSTEM_INCLUDED_20121230015033
#define H_ENTITYSYSTEM_INCLUDED_20121230015033

#include "EntitySystemDefines.h"
#include "ComponentID.h"
#include "Entity.h"
#include <vector>
#include <algorithm>
#include <type_traits>

namespace ES
{
    class EntitySystemBase
    {
    public:
        virtual ~EntitySystemBase() {}

		virtual void EntityComponentsUpdated(Entity *ent) = 0;

		virtual void EntityRemoved(const Entity *ent) = 0;

        virtual void Update(const float time) = 0;
    };

	template<typename BaseClass, typename ...Components>
    class EntitySystem : public EntitySystemBase
	{
	public:
		
		EntitySystem()
		{
            BuildRequiredComponentList<Components...>();
            BuildUpdateStruct<Components..., SeperatorComponent>();
		}

		virtual ~EntitySystem() {}	

		//virtual void UpdateEntity(Entity *ent, const float time, Components &...cmps) = 0;

		/** Will be called before looping throw all entities
			@param time The time since the last frame
			@return true indicates to loop throw all entities and false indicates to do nothing
		*/
		virtual bool PreUpdate(const float time) { return true; }

		virtual void PostUpdate() {}

	protected:	  
		std::vector<Entity*> mEntities;

	private:   		

		struct UpdateStructBase
		{
            virtual void Update(EntitySystem<BaseClass, Components...> *sys, const float time) = 0;
		};

		UpdateStructBase *pUpdate;

        template<typename ...UpdateComponents>
		struct UpdateStruct : public UpdateStructBase
		{
            void Update(EntitySystem<BaseClass, Components...> *sys, const float time)
			{
                if(!sys->PreUpdate(time))
					return;

                for(auto itr(sys->mEntities.begin()), end(sys->mEntities.end()); itr != end; ++itr)
                    static_cast<BaseClass*>(sys)->UpdateEntity(*itr, time, static_cast<Entity*>(*itr)->GetComponent<UpdateComponents>()...);

                sys->PostUpdate();
			}
		};

        class SeperatorComponent
		{};

        template<typename FirstComponent, typename ...RestComponents>
        typename std::enable_if<std::is_same<FirstComponent, SeperatorComponent>::value, void>::type
        BuildUpdateStruct()
		{
			pUpdate = new UpdateStruct<RestComponents...>();
		}

        template<typename FirstComponent, typename ...RestComponents>
        typename std::enable_if<std::is_base_of<ComponentFromEnumBase, FirstComponent>::value, void>::type
        BuildUpdateStruct()
		{
			BuildUpdateStruct<RestComponents...>();
		}

        template<typename FirstComponent, typename ...RestComponents>
        typename std::enable_if<!std::is_same<FirstComponent, SeperatorComponent>::value && !std::is_base_of<ComponentFromEnumBase, FirstComponent>::value, void>::type
        BuildUpdateStruct()
		{
			BuildUpdateStruct<RestComponents..., FirstComponent>();
		}

        void Update(const float time)
		{
            pUpdate->Update(this, time);
		}
		
		void EntityComponentsUpdated(Entity *ent)
		{
			const auto &components = ent->GetComponents();

			bool alreadyexist = std::find(mEntities.begin(), mEntities.end(), ent) != mEntities.end();

			for(auto itr(mRequiredComponents.begin()), end(mRequiredComponents.end()); itr != end; ++itr)
				if(components.find(*itr) == components.end())
				{
					// if the entity was already added, but now an important component was removed, so remove the entity too!
					if(alreadyexist)
						EntityRemoved(ent);

					return;
				}

				if(!alreadyexist)
					mEntities.push_back(ent);
		}

		void EntityRemoved(const Entity *ent)
		{
			auto itr = std::find(mEntities.begin(), mEntities.end(), ent);

			if(itr != mEntities.end())
				mEntities.erase(itr);
		}

		template<typename First>
		void BuildRequiredComponentList()
		{
			mRequiredComponents.push_back(ComponentIDGenerator<First>::Get());
		}

		template<typename First, typename Second, typename ...Rest>
		void BuildRequiredComponentList()
		{
			mRequiredComponents.push_back(ComponentIDGenerator<First>::Get());
			BuildRequiredComponentList<Second, Rest...>();
		}

		std::vector<ComponentID> mRequiredComponents;
	};
}   //namespace ES

#endif //#ifndef H_ENTITYSYSTEM_INCLUDED_20121230015033
