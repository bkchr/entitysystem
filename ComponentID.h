#ifdef PRAGMA_ONCE_ENABLED
#   pragma once
#endif

#ifndef H_COMPONENTID_INCLUDED_20121230004700
#define H_COMPONENTID_INCLUDED_20121230004700

#include "EntitySystemDefines.h"

namespace ES
{
	class ComponentIDCounter	
	{
	public:
		static ComponentID GetNextID() { return mIDCounter++; } 

	private:
		static ComponentID mIDCounter;
	};

	template<typename Component>
	class ComponentIDGenerator
	{
	public:
		static ComponentID Get() { return mID; }

	private:
		const static ComponentID mID;
	};

	template<typename Component>
	const ComponentID ComponentIDGenerator<Component>::mID = ComponentIDCounter::GetNextID();
}   //namespace ES

#endif //#ifndef H_COMPONENTID_INCLUDED_20121230004700