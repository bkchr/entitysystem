#ifdef PRAGMA_ONCE_ENABLED
#   pragma once
#endif

#ifndef H_ENTITYSYSTEMMANAGER_INCLUDED_20121229154758
#define H_ENTITYSYSTEMMANAGER_INCLUDED_20121229154758

#include "EntitySystemDefines.h"
#include "ComponentStore.h"
#include "ComponentID.h"
#include "Components.h"
#include "Entity.h"
#include "EntitySystem.h"
#include <tuple>
#include <vector>

namespace ES
{
    class EntitySystemManager
	{
	public:
		EntitySystemManager() : mNextEntityID(0) {}
		~EntitySystemManager()
		{
			mSystems.clear();
			mEntities.clear();
		}

		Entity *CreateEntity()
		{
			mEntities.push_back(new Entity(mNextEntityID++));

			return mEntities.back();
		}

		void RemoveEntity(const Entity *ent)
		{
			auto itr = std::find(mEntities.begin(), mEntities.end(), ent);

			if(itr == mEntities.end())
				return;

			mEntities.erase(itr);
		}

        void RegisterSystem(EntitySystemBase *system, unsigned char order = 128)
		{
			mSystems.insert(std::pair<unsigned char, EntitySystemBase*>(order, system));

			for(auto itr(mEntities.begin()), end(mEntities.end()); itr != end; ++itr)
				system->EntityComponentsUpdated(*itr);
		}

        void RemoveSystem(EntitySystemBase *system)
		{
			for(auto itr(mSystems.begin()), end(mSystems.end()); itr != end; ++itr)
				if(itr->second == system)
				{
					mSystems.erase(itr);
					return;
				}
		}

		void Update(const float time)
		{
			for(auto itr(mEntities.begin()), end(mEntities.end()); itr != end; ++itr)
				if((*itr)->ComponentsDirty(true))
					InformSystemsAboutComponentUpdate(*itr);

			for(auto itr(mSystems.begin()), end(mSystems.end()); itr != end; ++itr)
				itr->second->Update(time);
		}

	private:
		// Stores the Entities with their Components
		std::vector<Entity*> mEntities;

		void InformSystemsAboutComponentUpdate(Entity *ent)
		{
			for(auto itr(mSystems.begin()), end(mSystems.end()); itr != end; ++itr)
				itr->second->EntityComponentsUpdated(ent);
		}

		void InformSystemAboutEntityRemoving(const Entity *ent)
		{
			for(auto itr(mSystems.begin()), end(mSystems.end()); itr != end; ++itr)
				itr->second->EntityRemoved(ent);
		} 	

		unsigned long mNextEntityID;

        std::unordered_multimap<unsigned char, EntitySystemBase*> mSystems;
	};
}   //namespace ES

#endif //#ifndef H_ENTITYSYSTEMMANAGER_INCLUDED_20121229154758
