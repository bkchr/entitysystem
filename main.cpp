#include <iostream>
#include <SFML/Graphics.hpp>
#include "EntitySystemManager.h"
#include "EntitySystem.h"

class SpriteComponent
{
public:
	SpriteComponent(float radius) : mRadius(radius) {}

	float GetRadius() {return mRadius;}

private:
	float mRadius;
};

class PositionComponent
{
	
public:
	float mX,mY;

	PositionComponent(float x, float y) : mX(x), mY(y) {} 
};

class InputComponent
{

};

class VelocityComponent
{
public:
	float mX, mY;

	VelocityComponent(float x = 0, float y = 0) : mX(x), mY(y) {}
};

ES::EntitySystemManager essystem;

class SpriteSystem : public ES::EntitySystem<SpriteComponent, PositionComponent>
{
	sf::CircleShape sprite;
	sf::RenderWindow *pwindow;
public:
	SpriteSystem(sf::RenderWindow *window) : ES::EntitySystem<SpriteComponent, PositionComponent>(&essystem), sprite(1.f), pwindow(window) {sprite.setFillColor(sf::Color::Green);}

	virtual void UpdateEntity(const ES::Entity ent, const float time, SpriteComponent &sp, PositionComponent &pos)
	{
		sprite.setRadius(sp.GetRadius());
		sprite.setPosition(pos.mX, pos.mY);

		pwindow->draw(sprite);
	}
};

class InputSystem : public ES::EntitySystem<InputComponent, VelocityComponent>
{
public:
	InputSystem() : ES::EntitySystem<InputComponent, VelocityComponent>(&essystem) {}

	virtual void UpdateEntity(const ES::Entity ent, const float time, InputComponent &input, VelocityComponent &vel)
	{
		vel.mX = vel.mY = 0;
		float move = 30;

		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			vel.mX = -move;
		else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			vel.mX = move;

		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			vel.mY = -move;
		else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			vel.mY = move;
	}
};

class MovementSystem : public ES::EntitySystem<PositionComponent, VelocityComponent>
{
public:
	MovementSystem() : ES::EntitySystem<PositionComponent, VelocityComponent>(&essystem) {}

	virtual void UpdateEntity(const ES::Entity ent, const float time, PositionComponent &pos, VelocityComponent &vel)
	{
		pos.mX += vel.mX * time;
		pos.mY += vel.mY * time;	
	}
};

int main(int argc, char* argv[])   
{
	sf::RenderWindow window(sf::VideoMode(200, 200), "SFML works!");
	
	SpriteSystem system(&window);
	InputSystem isystem;
	MovementSystem msystem;
	essystem.RegisterSystem(&system);
	essystem.RegisterSystem(&isystem);
	essystem.RegisterSystem(&msystem);

	auto ent = essystem.CreateEntity();
	essystem.AddComponent<SpriteComponent>(ent, 10.f);
	essystem.AddComponent<PositionComponent>(ent, 10.f, 50.f);
	essystem.AddComponent<InputComponent>(ent);
	essystem.AddComponent<VelocityComponent>(ent);

	auto ent2 = essystem.CreateEntity();
	essystem.AddComponent<SpriteComponent>(ent2, 50.f);
	essystem.AddComponent<PositionComponent>(ent2, 50.f, 10.f);

	sf::Clock clock;
	while (window.isOpen())
	{
		sf::Time elapsed = clock.restart();
		sf::Event event;
		while (window.pollEvent(event))
		{				
			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear();

		essystem.Update(elapsed.asSeconds());
		
		window.display();
	}

	return 0;
}